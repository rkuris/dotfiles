#eval $(thefuck --alias)
alias h='fc -lDf 1'
setopt RM_STAR_WAIT
use() {
	if [[ ! -d "$1" || ! -d "$1/src" ]]; then
		echo "$1 doesn't look like a valid go project" 1>&2
		return
	fi

	local -Ua newpath

	for item in $path; do
		if [[ ! "$item" == ~/src* && -d "$item" ]]; then
			newpath=($newpath $item)
		fi
	done
	export GOPATH=$1
	newpath=($1/bin $newpath)
	path=($newpath)
	if [[ ! "$PWD" == $1* ]]; then
		cd $1
	fi
}
cql() {
	local cass=~/open-source/apache-cassandra-3.9
	if [[ ! -x "$(whence -p cqlsh)" ]] ; then
		# fix up the path
		path=($cass/bin $path)
	fi
	if [[ -z "$(nc -z -w5 localhost 9042 2>&1)" ]] ; then
		$cass/bin/cassandra
	fi
	cqlsh "$@"
}

upall() {
	pushd ~/fbsource/fbcode
	hg pull && hg rebase -d master
	cd ~/configerator
	hg pull && hg rebase -d master
	popd
}

[[ -d ~/configerator ]] && hash -d conf=~/configerator
[[ -d ~/fbsource/fbcode ]] && hash -d fbcode=~/fbsource/fbcode
