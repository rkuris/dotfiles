# added by newengsetup
export EDITOR=vim
typeset -U path
path+=('/usr/local/bin' "$HOME/bin")

if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

if [[ -d /usr/local/opt/android-sdk ]]; then
	export ANDROID_HOME=/usr/local/opt/android-sdk
	export ANDROID_NDK=$(brew --prefix android-ndk)
fi

for path_consider ('/usr/local/opt/python/libexec/bin' '/usr/local/opt/python/libexec/bin', "$HOME/.cargo/bin")
do
    if [[ -d "$path_consider" ]]; then
	path+=($path_consider)
    fi
done

if [[ -d "$HOME/perl5" ]]; then
  eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib=$HOME/perl5)"
fi

if [[ -d '/usr/local/opt/ruby' ]]; then
  export LDFLAGS="-L/usr/local/opt/ruby/lib"
  export CPPFLAGS="-I/usr/local/opt/ruby/include"
  export PKG_CONFIG_PATH="/usr/local/opt/ruby/lib/pkgconfig"
fi
