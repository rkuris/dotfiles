let fbspecial = expand("$LOCAL_ADMIN_SCRIPTS/master.vimrc")
if filereadable(fbspecial)
  exec 'source ' . fbspecial
endif

"set expandtab shiftwidth=4 softtabstop=4
let g:go_disable_autoinstall = 0
filetype off

" Highlight
let g:go_highlight_functions = 1  
let g:go_highlight_methods = 1  
let g:go_highlight_structs = 1  
let g:go_highlight_operators = 1  
let g:go_highlight_build_constraints = 1  

call plug#begin('~/.vim/plugged')
" Make sure you use single quotes
Plug 'junegunn/seoul256.vim'
Plug 'junegunn/vim-easy-align'
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
Plug 'fatih/vim-go'
Plug 'nsf/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
Plug 'Shougo/neocomplete'
Plug 'tpope/vim-fugitive'
Plug 'nvie/vim-flake8'
call plug#end()
filetype plugin indent on

" autocmd FileType go setlocal omnifunc=go#complete#Complete
map <F1> :GoBuild
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" <TAB>: completion.

let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

autocmd filetype crontab setlocal nobackup nowritebackup

" Go related mappings
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>r <Plug>(go-run)
au FileType go nmap <Leader>b <Plug>(go-build)
au FileType go nmap <Leader>t <Plug>(go-test)
au FileType go nmap gd <Plug>(go-def-tab)

let s:tlist_def_go_settings = 'go;g:enum;s:struct;u:union;t:type;' .
                           \ 'v:variable;f:function'

let g:netrw_liststyle = 3
nnoremap <Leader><Leader> :Tlist<CR><C-W>h<C-W>s:e .<CR><C-W>l:let g:netrw_chgwin=winnr()<CR><C-W>h
